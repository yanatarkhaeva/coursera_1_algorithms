def merge_lists(left, right):
    result = []
    while len(left) or len(right):
        if len(left) == 0:
            result.append(right.pop(0))
        elif len(right) == 0:
            result.append(left.pop(0))
        elif left[0] > right[0]:
            result.append(right.pop(0))
        elif left[0] < right[0]:
            result.append(left.pop(0))
        else:
            result.append(left.pop(0))
            right.pop(0)
    return result


def merge_sort(list_to_sort):
    if len(list_to_sort) == 1:
        return list_to_sort
    half_mark = int(len(list_to_sort)/2)
    sorted_left = merge_sort(list_to_sort[:half_mark])
    sorted_right = merge_sort(list_to_sort[half_mark:])
    result = merge_lists(sorted_left, sorted_right)
    return result


def main():
    lists_to_sort = [
        [1, 2, 4, 3],
        [13, 1456, 1, 45, 7, 23, 24, 5],
        [54, 26, 93, 17, 77, 31, 44, 55, 20]
    ]
    for list_to_sort in lists_to_sort:
        print(f"let's have {list_to_sort} sorted")
        print(merge_sort(list_to_sort))


if __name__ == '__main__':
    main()
